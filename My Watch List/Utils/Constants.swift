//
//  Constants.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation

// MARK: - Constants related to Notifications
let yearSelectionChangedNotification =
    "com.imdb.yearSelectionChangedNotification"
let reloadWatchListNotification =
    "com.imdb.reloadWatchListNotification"


// MARK: - Constants related to View Controller Titles
let moviesWatchListViewControllerTitle = "My Watch List"
let imDbSeachMoviesViewControllerTitle = "Seach Movies"
let iMDbViewControllerTitle = "Movie Details"


let dummyPosterUrlString = "https://cdn.iconscout.com/icon/free/png-256/movie-154-287978.png"
