//
//  GenericPickerDataSource.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

class GenericPickerDataSource<T>: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
    
    public var originalItems: [T]
    public var items: [GenericPickerRow<T>]
    public var selected: (T) -> Void
    
    public init(withItems originalItems: [T], withRowTitle generateRowTitle: (T) -> String, didSelect selected: @escaping (T) -> Void) {
        self.originalItems = originalItems
        self.selected = selected
        
        self.items = originalItems.map {
            GenericPickerRow<T>(type: $0, title: generateRowTitle($0))
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return items[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.selected(items[row].type)
        
    }
    
}
