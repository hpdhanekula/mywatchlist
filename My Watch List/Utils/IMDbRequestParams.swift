//
//  IMDbParams.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation

enum IMDbRequestParam: String {
    
    case apiKey
    
    /// Optional*        <empty>    A valid IMDb ID (e.g. tt1285016)
    case imDbID = "i"
    
    /// Optional*        <empty>    Movie title to search for.
    case title = "t"
    
    /// Optional  movie, series, episode    <empty>    Type of result to return.
    case type
    
    ///  Optional  <empty>    Year of release.
    case year = "y"
    
    ///  Optional short, full    short    Return short or full plot.
    case plot
    
    /// optional json, xml    json    The data type to return.
    case returnType = "r"
    
    ///  Optional short, full    short    Return short or full plot.
    enum plotType: String{
        case full
        case short
    }
    
    enum types: String {
        case movie
        case series
        case episode
    }
}
