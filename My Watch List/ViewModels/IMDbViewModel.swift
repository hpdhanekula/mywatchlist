//
//  IMDbViewModel.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import RealmSwift

// MARK: - IMDbViewModel
struct IMDbViewModel {
    
    private let realm = try? Realm()
    
    var getMediaLibItem: IMDbResponse?
    var isAddedToWatchList: Bool?

    func addToWatchList(completionHandler: @escaping () -> Void) {
        let movie = Movie()
        movie.name = getMediaLibItem?.title
        movie.posterUrl = getMediaLibItem?.posterUrl
        movie.rating = getMediaLibItem?.imdbRating
        movie.year = getMediaLibItem?.year
        movie.imdbID = getMediaLibItem?.imdbId
        
        // MARK: - Insert or Update Movie
        
        try? realm?.write{
            realm?.add(movie, update: Realm.UpdatePolicy.modified)
            completionHandler()
        }
        
    }
}
