//
//  MovieWatchListViewModel.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import RealmSwift

class MovieWatchListViewModel {
    let realm = try? Realm()
    
    var movieWatchList : Results<Movie>? {
        didSet {
            getNumberOfMovies = movieWatchList?.count ?? -1 > 0  ? movieWatchList?.count : 1
        }
    }
    
    var getNumberOfMovies: Int?
    
    var imDbServiceResponse : [IMDbResponse?]?
    
    init() {
        //print(realm?.configuration.fileURL)
        getMovieListData()
    }
    
    // MARK: - Get Movie Watch List from Local DB
    func getMovieListData() {
        
        movieWatchList =  realm?.objects(Movie.self).sorted(byKeyPath: "name", ascending: true)
    }
    
    func getMovieItem(at: Int) -> Movie? {
        return movieWatchList?.count ?? -1 > 0  ? movieWatchList?[at] : nil
    }
    
    // MARK: - Get Movie List based on IMDBID and year of release
    
    func getMovieListData(by imDbID: String, year: String, completionHandler:  @escaping() -> () ){
        
        var queryParams: [URLQueryItem]? = []
        
        
        queryParams?.append(URLQueryItem(name: IMDbRequestParam.imDbID.rawValue, value: imDbID ))
        if let selectedYear = Int(year), selectedYear > 0 {
            queryParams?.append(URLQueryItem(name: IMDbRequestParam.year.rawValue, value: year ))
        }
        
        IMDbServiceProvider().getIMDbResponse(queryParams) { [weak self] (imDbResponse) in
            self?.imDbServiceResponse = [imDbResponse]
            completionHandler()
        }
        
        
    }
    
    func prepareIMDbViewModelForMovie(at: Int) -> IMDbViewModel {
        let iMDbViewModel = IMDbViewModel(getMediaLibItem: imDbServiceResponse?[at], isAddedToWatchList: true)
        return iMDbViewModel
    }
}
