//
//  IMDbSearchViewModel.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import RealmSwift

// MARK: - IMDbSearchViewModel
class IMDbSearchViewModel {
    
    let realm = try? Realm()
    
    let yearData: Years
    
    var selectedYear: String = "Any Year" {
        didSet {
            NotificationCenter.default.post(name: Notification.Name(rawValue: yearSelectionChangedNotification), object: self)
        }
    }
    typealias Year = String
    var dataSource:GenericPickerDataSource<Year>?
    
    /// Current service only returing one Movie Object, but still cusidering it as list to convert this easyly in future to a list by modifying the API
    var imDbServiceResponse : [IMDbResponse?]? {
        didSet {
            getNumberOfMovies = imDbServiceResponse?.count ?? 0
        }
    }
    
    var getNumberOfMovies: Int?
    
    
    init() {
        let calender = Calendar.current
        let year = calender.component(.year, from: Date())
        yearData = Years(numberOfYears: 30, fromYear: year)
        
        dataSource = GenericPickerDataSource<Year>(
            withItems: yearData.yearsData,
            withRowTitle: {
                String($0)
            },
            didSelect: {
                self.selectedYear = $0
            }
        )
    }
    
    
    // MARK: - Get Movie List based on name and year of release
    
    func getMovieListData(by name: String, year: String,  completionHandler:  @escaping() -> () ){
        
        var queryParams: [URLQueryItem]? = []
        

        queryParams?.append(URLQueryItem(name: IMDbRequestParam.title.rawValue, value: name ))
        if let selectedYear = Int(year), selectedYear > 0 {
            queryParams?.append(URLQueryItem(name: IMDbRequestParam.year.rawValue, value: year ))
        }
        
        IMDbServiceProvider().getIMDbResponse(queryParams) { [weak self] (imDbResponse) in
            self?.imDbServiceResponse = [imDbResponse]
            completionHandler()
        }
        
        
    }
    
    // MARK: - Movie Item to render
    
    func getMovieItem(at: Int) -> IMDbResponse? {
        return imDbServiceResponse?[at]
    }
    
    // MARK:- Prepare IMDb View Model for Selected Movie
    
    func prepareIMDbViewModelForMovie(at: Int) -> IMDbViewModel {
        let iMDbViewModel = IMDbViewModel(getMediaLibItem: imDbServiceResponse?[at], isAddedToWatchList: false)
        return iMDbViewModel
    }
}

