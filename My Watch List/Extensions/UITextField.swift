//
//  UITextField.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

extension UITextField {
    
    func setupPickerField<T>(withDataSource dataSource: GenericPickerDataSource<T>) {
        
        let pickerView = UIPickerView()
        self.inputView = pickerView
        self.addDoneToolbar()
        
        pickerView.delegate = dataSource
        pickerView.dataSource = dataSource
    }
    
    func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil) {
        
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.tintColor = UIColor.systemBlue
        
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc private func doneButtonTapped() { self.resignFirstResponder() }
}
