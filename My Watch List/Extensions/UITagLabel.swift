//
//  UITagLabel.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

/// UILabel Subclass to render the tag line text

class UITagLabel :UILabel{
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.font = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize)
        self.numberOfLines = 0
        self.textColor = .secondaryLabel
        
    }
}
