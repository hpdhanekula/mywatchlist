//
//  IMDbServiceProvider.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation

class IMDbServiceProvider {
    
    static let apiKey = "c51d6a46"
    let urlSession = URLSession.shared
    
    var urlComponents: URLComponents
    
    init() {
        urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "www.omdbapi.com"
        urlComponents.path = "/"
        urlComponents.queryItems = [URLQueryItem(name: IMDbRequestParam.apiKey.rawValue,value: IMDbServiceProvider.apiKey)]
    }
    
    // MARK: - Get Movie
    func getIMDbResponse(_ queryParams: [URLQueryItem]?, completionHandler: @escaping (IMDbResponse?) -> Void) {
        if var queryItems = queryParams {
            queryItems.append(URLQueryItem(name: IMDbRequestParam.plot.rawValue, value: IMDbRequestParam.plotType.full.rawValue))
            queryItems.append(URLQueryItem(name: IMDbRequestParam.type.rawValue, value: IMDbRequestParam.types.movie.rawValue))
            urlComponents.queryItems?.append(contentsOf: queryItems)
        }
        
        guard let url = urlComponents.url else {
            return
        }
        let task = urlSession.dataTask(with: url) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, let responseData = data, httpResponse.statusCode == 200 else {
                print("Error", String(describing: error?.localizedDescription))
                print("Response", response as Any)
                return
            }
            let imDbResponse = try? JSONDecoder().decode(IMDbResponse.self, from: responseData)
            completionHandler(imDbResponse)
        }
        task.resume()
        
    }
    
}
