//
//  GenericPickerRow.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation

// MARK: - GenericPickerRow

struct GenericPickerRow<T> {
    let type: T
    let title: String
}
