//
//  Movie.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import RealmSwift

// MARK: - Movie

class Movie: Object {
    @objc dynamic var name: String?
    @objc dynamic var imdbID: String?
    @objc dynamic var posterUrl: String?
    @objc dynamic var year: String?
    @objc dynamic var rating: String?
    override static func primaryKey() -> String? {
        return "imdbID"
    }
}
