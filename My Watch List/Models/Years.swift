//
//  Years.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation


// MARK: - Years
struct Years {
    
    /// Number of years to capture
    var numberOfYears: Int
    
    /// Begining of the year
    var fromYear: Int
    
    /// Return years list in desc order from year
    var yearsData: [String] {
        get {
            var yearsData = ["Any Year"]
            for i in fromYear-numberOfYears...fromYear {
                yearsData.insert(String(i), at: 1)
            }
            return yearsData
        }
    }
}
