//
//  IMDbTableVewCell.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

class IMDbTableVewCell: UITableViewCell {
    
    static var identifier: String {
        String(describing: self)
    }
    let moviewNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)
        label.adjustsFontForContentSizeCategory = true
        return label
    }()
    
    let ratingLbl: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)
        label.adjustsFontForContentSizeCategory = true
        return label
    }()
    
    var ratingStar: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "star.fill")
        return imageView
    }()
    
    var poster: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    private lazy var ratingStackView:  UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = 5
        return stackView
    }()
    
    private lazy var iMDbCellStackView:  UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 5
        return stackView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        ratingStackView.addArrangedSubview(ratingStar)
        ratingStackView.addArrangedSubview(ratingLbl)
        iMDbCellStackView.addArrangedSubview(poster)
        iMDbCellStackView.addArrangedSubview(moviewNameLabel)
        contentView.addSubview(iMDbCellStackView)
        contentView.addSubview(ratingStackView)
        setupConstraints()
    }
    
    func setupConstraints(){
        iMDbCellStackView.translatesAutoresizingMaskIntoConstraints = false
        ratingStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            poster.widthAnchor.constraint(equalToConstant: 80),
            poster.heightAnchor.constraint(equalToConstant: 80),
//            iMDbCellStackView.heightAnchor.constraint(greaterThanOrEqualToConstant: 85),
            iMDbCellStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            iMDbCellStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            iMDbCellStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            iMDbCellStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -50),
            ratingStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
            ratingStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            ratingStackView.rightAnchor.constraint(equalTo: contentView.rightAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
