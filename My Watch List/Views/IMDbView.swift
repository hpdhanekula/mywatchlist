//
//  IMDbView.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

class IMDbView: UIView, UISearchBarDelegate {
    
    private var imDbViewModel: IMDbViewModel?
    
    public var isAddedToWachList: Bool? {
        get {
            return imDbViewModel?.isAddedToWatchList
        }
    }
    
    // TODO: - Calculate height dynamically
    public var getContentHeight:CGFloat = 1000
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: UIFont.systemFontSize)
        label.textColor = .label
        return label
    }()
    
    private lazy var ratedLabel: UITagLabel = {
        let label = UITagLabel()
        return label
    }()
    
    private lazy var releasedLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    private lazy var runtimeLabel: UITagLabel = {
        let label = UITagLabel()
        return label
    }()
    
    private lazy var genreLabel: UITagLabel = {
        let label = UITagLabel()
        return label
    }()
    private lazy var ratingStar: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .clear
        imageView.image = UIImage(systemName: "star.fill")
        return imageView
    }()
    
    private lazy var poster: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    
    private lazy var directorLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.textColor = .label
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var writerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.textColor = .label
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var actorsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.textColor = .label
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var plotLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize)
        label.numberOfLines = 0
        label.textColor = .label
        return label
    }()
    
    
    
    private let tagDetailsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        return stackView
    }()
    
    private let posterStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .center
        //stackView.heightAnchor.constraint(equalToConstant: 250).isActive = true
        return stackView
    }()
    
    private let moivePropertiesStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        return stackView
    }()
    
    private let imDbDetailstackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .top
        stackView.spacing = 5
        return stackView
    }()
    
    init(viewModel: IMDbViewModel){
        super.init(frame: .zero)
        
        self.imDbViewModel = viewModel
        
        imDbDetailstackView.addArrangedSubview(titleLabel)
        
        tagDetailsStackView.addArrangedSubview(ratingStar)
        tagDetailsStackView.addArrangedSubview(ratedLabel)
        //tagDetailsStackView.addArrangedSubview(runtimeLabel)
        // tagDetailsStackView.addArrangedSubview(genreLabel)
        
        imDbDetailstackView.addArrangedSubview(tagDetailsStackView)
        
        posterStackView.addArrangedSubview(poster)
        posterStackView.addArrangedSubview(moivePropertiesStackView)
        moivePropertiesStackView.addArrangedSubview(directorLabel)
        moivePropertiesStackView.addArrangedSubview(writerLabel)
        moivePropertiesStackView.addArrangedSubview(actorsLabel)
        //moivePropertiesStackView.addArrangedSubview()
        imDbDetailstackView.addArrangedSubview(posterStackView)
        moivePropertiesStackView.addArrangedSubview(plotLabel)
        
        addSubview(imDbDetailstackView)
        tagDetailsStackView.translatesAutoresizingMaskIntoConstraints = false
        posterStackView.translatesAutoresizingMaskIntoConstraints = false
        moivePropertiesStackView.translatesAutoresizingMaskIntoConstraints = false
        imDbDetailstackView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
        DispatchQueue.main.async {[weak self] in
            self?.setupData()
        }
        
        
    }
    
    func setupData() {
        titleLabel.text = "\(imDbViewModel?.getMediaLibItem?.title ?? "") (\(imDbViewModel?.getMediaLibItem?.year ?? ""))"
        ratingStar.image = UIImage(systemName: "star.fill")
        ratedLabel.text = "\(imDbViewModel?.getMediaLibItem?.imdbRating ?? "") | \(imDbViewModel?.getMediaLibItem?.runtime ?? "") | \(imDbViewModel?.getMediaLibItem?.genre ?? "")"
        //runtimeLabel.text = "\(imDbViewModel?.getMediaLibItem?.runtime ?? "") |"
        // genreLabel.text = "\(imDbViewModel?.getMediaLibItem?.genre ?? "")"
        directorLabel.text = "Director: \(imDbViewModel?.getMediaLibItem?.director ?? "")"
        actorsLabel.text = "Actors: \(imDbViewModel?.getMediaLibItem?.actors ?? "")"
        writerLabel.text = "Writer: \(imDbViewModel?.getMediaLibItem?.writer ?? "")"
        plotLabel.text = "\n\(imDbViewModel?.getMediaLibItem?.plot ?? "")"
        if let url = URL(string: imDbViewModel?.getMediaLibItem?.posterUrl ?? "")  {
            poster.load(url: url)
        }
        
    }
    func setupConstraints() {
        NSLayoutConstraint.activate([
            imDbDetailstackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            imDbDetailstackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
            imDbDetailstackView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            //imDbDetailstackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func addToWatchList(completionHandler: @escaping () -> Void){
        imDbViewModel?.addToWatchList { [weak self] in
            self?.imDbViewModel?.isAddedToWatchList = true
            completionHandler()
        }
    }
    
    func displayAlert(msg: String) -> UIAlertController{
    let alert = UIAlertController(title: "info", message: msg, preferredStyle: .alert)
     return alert
    }
}

