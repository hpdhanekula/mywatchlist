//
//  MovieWatchListView.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

class MovieWatchListView: UIView {
    private let movieWatchListViewModel = MovieWatchListViewModel()
    
    public var delegate: MoviesWatchListViewControllerProtocol?
    
    private lazy var moviesWatchListTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(IMDbTableVewCell.self, forCellReuseIdentifier: IMDbTableVewCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableView.automaticDimension
        tableView.translatesAutoresizingMaskIntoConstraints = true
        return tableView
    }()
    
    private let refreshControl = UIRefreshControl()
    
    init(){
        super.init(frame: .zero)
        moviesWatchListTableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(reloadWatchListData), for: .valueChanged)
        addSubview(moviesWatchListTableView)
        moviesWatchListTableView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadWatchListData), name: Notification.Name(rawValue: reloadWatchListNotification), object: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: reloadWatchListNotification), object: nil)
    }
    @objc func reloadWatchListData(){
        movieWatchListViewModel.getMovieListData()
        DispatchQueue.main.async { [weak self] in
            self?.moviesWatchListTableView.reloadData()
            self?.moviesWatchListTableView.refreshControl?.endRefreshing()
        }
        
    }
    func setupConstraints(){
        NSLayoutConstraint.activate([moviesWatchListTableView.topAnchor.constraint(equalTo: topAnchor), moviesWatchListTableView.leftAnchor.constraint(equalTo: leftAnchor), moviesWatchListTableView.rightAnchor.constraint(equalTo: rightAnchor), moviesWatchListTableView.bottomAnchor.constraint(equalTo: bottomAnchor)])
    }
}

// TableView Delegate Methods
extension MovieWatchListView : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return movieWatchListViewModel.getNumberOfMovies ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IMDbTableVewCell.identifier, for: indexPath) as! IMDbTableVewCell
        func hideDetailCells(_ hide: Bool) {
            cell.ratingStar.isHidden = hide
            cell.poster.isHidden = hide
            cell.ratingLbl.isHidden = hide
        }
        if let movieItem = movieWatchListViewModel.getMovieItem(at: indexPath.row) {
            hideDetailCells(false)
            cell.moviewNameLabel.text = "\(movieItem.name ?? "" ) (\(movieItem.year ?? ""))"
            cell.ratingLbl.text = movieItem.rating
            if let url = URL(string: movieItem.posterUrl ?? dummyPosterUrlString) {
                cell.poster.load(url: url)
            }
        } else {
            cell.moviewNameLabel.text = "Your Watch List is Empty, Please search and add to the list"
            hideDetailCells(true)
        }
        return cell
    }
    
    // TODO: - Implement navigation to IMDb Details view
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let movieItem = movieWatchListViewModel.getMovieItem(at: indexPath.row) {
            movieWatchListViewModel.getMovieListData(by: movieItem.imdbID ?? "", year: movieItem.year ?? "") { [weak self] in
                if let weakSelf = self, weakSelf.movieWatchListViewModel.imDbServiceResponse?.count ?? 0 == 1 {
                    weakSelf.delegate?.navigateToMovieDetailVC(imDbViewModel: (weakSelf.movieWatchListViewModel.prepareIMDbViewModelForMovie(at: 0)))
                }
        }
        }
        print("Implement navigation to IMDb Details view")
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
