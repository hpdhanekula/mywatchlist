//
//  IMDbSearchView.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import Foundation
import UIKit

class IMDbSearchView: UIView, UISearchBarDelegate {
    
    private let imDbSearchViewModel = IMDbSearchViewModel()
    public  var delegate: IMDbSeachMoviesViewControllerProtocol?
    private lazy var yearStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        return stackView
    }()
    private lazy var iMDbSearchStackView:  UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.spacing = 5
        return stackView
    }()
    private lazy var searchBar: UISearchBar = {
        let searchbar = UISearchBar()
        searchbar.delegate = self
        searchbar.layer.borderWidth = 0
        searchbar.placeholder = "Type a movie name ..."
        return searchbar
    }()
    
    private lazy var yearTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Which year was the movie released? "
        label.font = UIFont.italicSystemFont(ofSize: UIFont.systemFontSize)
        return label
    }()
    
    
    private lazy var yearTextField : UITextField = {
        let textField = UITextField()
        textField.text = imDbSearchViewModel.selectedYear
        textField.textColor = .systemBlue
        textField.addDoneToolbar()
        //textField.setupPickerField(withDataSource: dataSource!)
        return textField
    }()
    
    private lazy var movieListTableView : UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(IMDbTableVewCell.self, forCellReuseIdentifier: IMDbTableVewCell.identifier)
        tableView.separatorStyle = .singleLine
        tableView.translatesAutoresizingMaskIntoConstraints = true
        return tableView
    }()
    
    init(){
        super.init(frame:.zero)
        //yearTextField.text = iMDbViewModel.selectedYear
        yearTextField.setupPickerField(withDataSource: imDbSearchViewModel.dataSource!)
        yearStackView.addArrangedSubview(yearTitleLabel)
        yearStackView.addArrangedSubview(yearTextField)
        yearStackView.translatesAutoresizingMaskIntoConstraints = false
        
        searchBar.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        iMDbSearchStackView.addArrangedSubview(yearStackView)
        //iMDbSearchStackView.addArrangedSubview(yearTextField)
        iMDbSearchStackView.addArrangedSubview(searchBar)
        //yearTextField.setupPickerField(withDataSource: pickerViewModel.)
        self.addSubview(iMDbSearchStackView)
        self.addSubview(movieListTableView)
        iMDbSearchStackView.translatesAutoresizingMaskIntoConstraints = false
        movieListTableView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
        NotificationCenter.default.addObserver(self, selector: #selector(yearSelectionChanged), name: Notification.Name(rawValue: yearSelectionChangedNotification), object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: yearSelectionChangedNotification), object: nil)
    }
    
    func setupConstraints() {
        NSLayoutConstraint.activate([iMDbSearchStackView.topAnchor.constraint(equalTo: topAnchor), iMDbSearchStackView.leftAnchor.constraint(equalTo: leftAnchor), iMDbSearchStackView.rightAnchor.constraint(equalTo: rightAnchor), iMDbSearchStackView.heightAnchor.constraint(equalToConstant: 80)])
        NSLayoutConstraint.activate([movieListTableView.topAnchor.constraint(equalTo: iMDbSearchStackView.bottomAnchor), movieListTableView.leadingAnchor.constraint(equalTo: iMDbSearchStackView.leadingAnchor), movieListTableView.trailingAnchor.constraint(equalTo: iMDbSearchStackView.trailingAnchor), movieListTableView.bottomAnchor.constraint(equalTo: bottomAnchor)])
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    @objc func yearSelectionChanged() {
        yearTextField.text = imDbSearchViewModel.selectedYear
    }
}



// TableView Delegate Methods
extension IMDbSearchView : UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return imDbSearchViewModel.getNumberOfMovies ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: IMDbTableVewCell.identifier, for: indexPath) as! IMDbTableVewCell
        cell.moviewNameLabel.text = imDbSearchViewModel.getMovieItem(at: indexPath.row)?.title ?? ""
        func hideDetailCells(_ hide: Bool) {
            cell.ratingStar.isHidden = hide
            cell.poster.isHidden = hide
            cell.ratingLbl.isHidden = hide
        }
        if let movieItem = imDbSearchViewModel.getMovieItem(at: indexPath.row) {
            if ((movieItem.response?.contains("False")) ?? false)  {
                cell.moviewNameLabel.text = movieItem.error
                hideDetailCells(true)
            } else {
                hideDetailCells(false)
                cell.moviewNameLabel.text = "\(movieItem.title ?? "" ) (\(movieItem.year ?? ""))"
                cell.ratingLbl.text = movieItem.imdbRating
                if let url = URL(string: movieItem.posterUrl ?? dummyPosterUrlString) {
                    cell.poster.load(url: url)
                }
            }
        } else {
            hideDetailCells(true)
            cell.moviewNameLabel.text = ""
        }
       return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let movieItem = imDbSearchViewModel.getMovieItem(at: indexPath.row){
            if !(movieItem.response?.contains("False") ?? false){
                delegate?.navigateToMovieDetailVC(imDbViewModel: imDbSearchViewModel.prepareIMDbViewModelForMovie(at: indexPath.row))
            }
        }
    }
}

// SearchBar Delegate methods
extension IMDbSearchView {
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !(searchBar.text?.isEmpty ?? true), let movieName = searchBar.text, let selectedYear = yearTextField.text else {
            return
        }
        imDbSearchViewModel.getMovieListData(by: movieName, year: selectedYear) { [weak self] in
            DispatchQueue.main.async {
                self?.movieListTableView.reloadData()
            }
        }
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let movieName = searchBar.text, let selectedYear = yearTextField.text else {
            return
        }
        imDbSearchViewModel.getMovieListData(by: movieName, year: selectedYear) { [weak self] in
            DispatchQueue.main.async {
                self?.movieListTableView.reloadData()
            }
        }
        searchBar.resignFirstResponder()
    }
    
}
