//
//  MoviesWatchListViewController.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import UIKit

protocol MoviesWatchListViewControllerProtocol {
    func navigateToMovieDetailVC(imDbViewModel: IMDbViewModel)
}
class MoviesWatchListViewController: UIViewController, MoviesWatchListViewControllerProtocol {
    
    let movieWatchListView = MovieWatchListView()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = moviesWatchListViewControllerTitle
        let rightBarButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(navigateToSearchMovie))
        navigationItem.rightBarButtonItem = rightBarButton
        movieWatchListView.delegate = self
        
        view.addSubview(movieWatchListView)
        view.backgroundColor = .systemBackground
        
        movieWatchListView.translatesAutoresizingMaskIntoConstraints = false
        setupConstraints()
    }
    
    func setupConstraints(){
        NSLayoutConstraint.activate([movieWatchListView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor), movieWatchListView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor), movieWatchListView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor), movieWatchListView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }
    
    @objc func navigateToSearchMovie() {
        navigationController?.pushViewController(IMDbSeachMoviesViewController(), animated: true)
    }
    func navigateToMovieDetailVC(imDbViewModel: IMDbViewModel) {
        DispatchQueue.main.async { [weak self] in
            let imDBViewController = IMDbViewController(viewModel: imDbViewModel)
            self?.navigationController?.pushViewController(imDBViewController, animated: true)
        }
        
    }
}
