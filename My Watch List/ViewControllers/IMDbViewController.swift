//
//  IMDbViewController.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import UIKit

class IMDbViewController: UIViewController {
    
    var imDbUIView: IMDbView?
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()
    init(viewModel: IMDbViewModel) {
        super.init(nibName: nil, bundle: nil)
        imDbUIView = IMDbView(viewModel: viewModel)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = iMDbViewControllerTitle
        guard let imDbView = imDbUIView else {
            return
        }
        
        setupRightBarButton(isAddedToWachList: imDbView.isAddedToWachList)
        
        scrollView.addSubview(imDbView)
        view.addSubview(scrollView)
        view.backgroundColor = .systemBackground
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        imDbView.translatesAutoresizingMaskIntoConstraints = false
        
        setupConstraints(for: imDbView)
    }
    
    func setupConstraints(for imDbView: IMDbView){
        
        NSLayoutConstraint.activate([scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor), scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),  scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            scrollView.heightAnchor.constraint(equalTo: view.heightAnchor)])
        NSLayoutConstraint.activate([imDbView.topAnchor.constraint(equalTo: scrollView.topAnchor), imDbView.leftAnchor.constraint(equalTo: scrollView.leftAnchor), imDbView.widthAnchor.constraint(equalTo: scrollView.widthAnchor), imDbView.heightAnchor.constraint(equalToConstant: imDbView.getContentHeight), imDbView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor)])
        print("getContentHeight", imDbView.getContentHeight)
    }
    
    func setupRightBarButton(isAddedToWachList: Bool?){
        if isAddedToWachList ?? false {
            navigationItem.rightBarButtonItem = nil
        } else {
        let rightBarButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon-add-list"), style: .plain, target: self, action: #selector(addToWatchList))
        navigationItem.rightBarButtonItem = rightBarButton
        }
        
    }
    @objc func addToWatchList(){
        imDbUIView?.addToWatchList {[weak self] in
            let successMsg = "Added Sucessfully. Would you like to go to watch list or Search?"
            if let alert = self?.imDbUIView?.displayAlert(msg: successMsg) {
                NotificationCenter.default.post(name: Notification.Name(rawValue: reloadWatchListNotification), object: self)
                alert.addAction(UIAlertAction(title: "Watch List", style: .default, handler: { (watchListAction) in
                    self?.navigationController?.popToRootViewController(animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Search", style: .default, handler: { (searchAction) in
                    self?.navigationController?.popViewController(animated: true)
                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (cancelAction) in
                    self?.setupRightBarButton(isAddedToWachList: self?.imDbUIView?.isAddedToWachList)
                }))
                self?.present( alert, animated: true)
            }
           
        }
    }
}
