//
//  IMDbSeachMoviesViewController.swift
//  My Watch List
//
//  Created by Harsha on 11/8/20.
//

import UIKit

protocol IMDbSeachMoviesViewControllerProtocol {
    func navigateToMovieDetailVC(imDbViewModel: IMDbViewModel)
}
class IMDbSeachMoviesViewController:UIViewController,  IMDbSeachMoviesViewControllerProtocol {
    
    let imDbSearcView = IMDbSearchView()
    override func viewDidLoad() {
        super.viewDidLoad()
        title = imDbSeachMoviesViewControllerTitle
        view.addSubview(imDbSearcView)
        view.backgroundColor = .systemBackground
        imDbSearcView.delegate = self
        imDbSearcView.translatesAutoresizingMaskIntoConstraints = false
        
        imDbSearcView.layoutIfNeeded()
        
        setupConstraints()
    }
    
    func setupConstraints(){
        NSLayoutConstraint.activate([imDbSearcView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor), imDbSearcView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor), imDbSearcView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor), imDbSearcView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
        
    }
    
    func navigateToMovieDetailVC(imDbViewModel: IMDbViewModel) {
        let imDBViewController = IMDbViewController(viewModel: imDbViewModel)
        navigationController?.pushViewController(imDBViewController, animated: true)
        
        print("Navigate to Movie Details ...")
    }
}
